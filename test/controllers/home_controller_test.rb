require 'test_helper'

class HomeControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get quienes_somos" do
    get :quienes_somos
    assert_response :success
  end

  test "should get solicitar_pqr" do
    get :solicitar_pqr
    assert_response :success
  end

  test "should get consultar_pqr" do
    get :consultar_pqr
    assert_response :success
  end

  test "should get iniciar_sesion" do
    get :iniciar_sesion
    assert_response :success
  end

end
