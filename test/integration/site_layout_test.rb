require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

  test "layout links" do
    get root_path
    assert_template 'views/home'
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", solicitar_pqr
    assert_select "a[href=?]", consultar_pqr
    assert_select "a[href=?]", iniciar_sesion
  end
end