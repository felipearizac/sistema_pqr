class HomeController < ApplicationController
  layout "application", :except=> [:quienes_somos, :solicitar_pqr, :consultar_pqr, :iniciar_sesion]
  
  
  
  def index
    @titulo = "Empresa ABC -Inicio"
  end

  def quienes_somos
    @titulo = "Empresa ABC -Quienes Somos"
  end

  def solicitar_pqr
    @titulo = "Empresa ABC -Solicitar PQRS"
   # if Email.recepcion.deliver
    #  flash.now[:notice] = "Se ha enviado el correo"
    #else
     # flash.now[:notice] = "no se ha enviado correctamente"
    #end
  end

  def consultar_pqr
    @titulo = "Empresa ABC - Consultar PQRS"
  end

  def iniciar_sesion
    @titulo = "Empresa ABC - Iniciar Sesion"
  end
end
