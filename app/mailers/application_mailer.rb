class ApplicationMailer < ActionMailer::Base
  default from: "solicitud@abc.com"
  layout 'mailer'
end
