module HomeHelper
	def enlace(nombre,accion)
		link_to nombre, :action=>accion
	end


	def menu_navegacion(opciones)
		html = '<ul class="nav nav-pills">'
		opciones.each do |nombre,accion|
			html += '<li>' +  enlace(nombre, accion)   + '</li>'
		end
		html += '</ul>'

		return html.html_safe

	end

end
